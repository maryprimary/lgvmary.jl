#=
常用的内容
=#

"""
高斯积分的作用量
"""
function action_Gaussian()
    @variables x
    return x, x^2
end


"""
带一个虚数的高斯积分
"""
function dissipative_action_Gaussian()
    @variables x, imagv
    return x, imagv, x^2-x*imagv+log(x)
end


"""
带一个虚数的高斯积分
"""
function fake_action()
    @variables x, imagv
    return x, imagv, x^2/1.1/exp(-0.05*imagv)-log((1.0-0.3*imagv)^2+(x-0.1*imagv)^2)
end
