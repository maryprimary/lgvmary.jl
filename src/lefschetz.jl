#=

=#


"""
反向的Morse flow
"""
function inv_Morse(S, ∂S_∂x, xsym, xval, syms, vars; T=2.0, steps=100)
    vardict = Dict()
    for sv in zip(syms, vars)
        vardict[sv[1]] = sv[2]
    end
    #println(vardict)
    xtvals = []
    dt = T/steps
    xt = xval
    push!(xtvals, xt)
    for idx in Base.OneTo(steps)
        vardict[xsym] = xt
        xdot = Symbolics.value(substitute(∂S_∂x, vardict))
        xdot = adjoint(xdot)
        xt = xt + xdot*dt
        push!(xtvals, xt)
    end
    #println(vardict)
    return xt, xtvals
end


"""
演化jacobian
"""
function inv_Morse_jacobian(S, Hes, jaci, xsym, xvals, syms, vars; T=2.0, steps=100)
    vardict = Dict()
    for sv in zip(syms, vars)
        vardict[sv[1]] = sv[2]
    end
    #
    dt = T/steps
    for x in xvals
        vardict[xsym] = x
        jdot = Symbolics.value(substitute(Hes, vardict))
        jdot = adjoint(jdot*jaci)
        jaci = jaci + dt*jdot
    end
    return jaci
end

