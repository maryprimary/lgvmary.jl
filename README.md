Langevin Dynamics & Lefschetz Thimbles
===


- [ ] 实现Generalize Thimble Approach，寻找积分的围道

<img src='./example_gta.png' width=400 height=300>


Notes
====

- 有些论文会用variance=2的噪音，因为他们用的dt会有个2倍

- ∂S/∂z和∂S(x+iy)/∂x是相同的（∂S(x+iy)/∂x=∂S(z)/∂z ∂z/∂x=∂S(z)/∂z?），arxiv:1101.3270，0912.3360用的后面的
