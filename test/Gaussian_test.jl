#=
测试高斯积分
=#


include("../src/lgvmary.jl")
using ..lgvmary

using Symbolics


#=
∫ e^{-x^2} dx = √π
∫ x^{2n} e^{-0.5ax^2} dx = (2n-1)!! sqrt(2π/a) / a^n 
n=1 a=2: √π/2
∫ x^{2} e^{-x^2} dx / ∫ e^{-x^2} dx = 1/2
=#
function run()
    

    x, S = action_Gaussian()
    O = x*exp(S)
    ∂S_∂x = Symbolics.derivative(S, x)
    #expand_derivatives(Differential(x)(S))

    println("$x $S $∂S_∂x $O")

    Sval = substitute(S, Dict(x => 5))
    #Symbolics.fixpoint_sub(S, Dict(x => 5))
    Spval = substitute(∂S_∂x, Dict(x => 5))
    #Symbolics.fixpoint_sub(∂S_∂x, Dict(x => 5))

    println("$Sval $Spval")

    println(rich_simpling(S, x^2))

     
    #res = 0.0
    #xval::Float64 = 0.0
    #t::Int = 0
    #nsp = 100000
    #for _ in Base.OneTo(nsp)
    #    t, xval, oval = langvein_step(t, xval, S, x^2)
    #    #println(va[1], typeof(va[1]))
    #    #println(va[2], typeof(va[2]))
    #    #println(va[3], typeof(va[3]))
    #    res += oval
    #end
    #println(res/nsp)
    #
    x, S = action_Gaussian()
    println(S, Symbolics.get_variables(S))
    v = Symbolics.substitute(S, Dict(real(x)=>1.0+0.1im))
    v2 = Symbolics.value(v)
    println(v, typeof(v), v2, typeof(v2))
    #O = x*exp(S)
    #∂S_∂x = Symbolics.derivative(S, x)
    #println(∂S_∂x)
    cres = 0.0
    cxval::ComplexF64 = 0.0+1.0im
    #t::Int = 0
    t = 0
    nsp = 100000
    for _ in Base.OneTo(nsp)
        t, cxval, oval = complex_langvein_step(t, cxval, S, x^2)
        #println(va[1], typeof(va[1]))
        #println(va[2], typeof(va[2]))
        #println(va[3], typeof(va[3]))
        cres += oval
    end
    println(cres/nsp)
end

run()
