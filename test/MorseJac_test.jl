#=
测试高斯积分
=#


include("../src/lgvmary.jl")
using ..lgvmary

using Symbolics


#=
∫ e^{-x^2} dx = √π
∫ x^{2n} e^{-0.5ax^2} dx = (2n-1)!! sqrt(2π/a) / a^n 
n=1 a=2: √π/2
∫ x^{2} e^{-x^2} dx / ∫ e^{-x^2} dx = 1/2
#
integral from -inf to inf x^2 e^{-x^2 + i*x} dx = √π/(4*e^{1/4}) ≈ 0.345097
int e^{-x^2 + i*x}  from -inf to inf = √π/e^{1/4}
∫ x^{2} e^{-x^2+x*im} dx / ∫ e^{-x^2+x*im} dx = 1/4

#
S = x^2 - x*im
∂S/∂x = 2x - im
0点 x = im/2由这个邻域生成thimble

S = x^2 - x*im + log(x)
∂S/∂x = 2x - im + 1/x
0点 x = i, -im/2由这个邻域生成thimble
=#

using Plots


function run()
    x, imagv, S = fake_action()
    O = x^2*exp(S)
    ∂S_∂x = Symbolics.derivative(S, x)
    #三个解
    #x = -0.69769 - 0.375713 i
    #x = 0.182357 + 0.0414223 i
    #0.515333 + 0.534291 i

    Hes = Symbolics.derivative(∂S_∂x, x)
    println(Hes)

    cv, path = inv_Morse(S, ∂S_∂x, x, -0.5, [imagv], [0.0+1.0im]; T=1.0)
    jaci = inv_Morse_jacobian(S, Hes, 1.0+0.0im, x, path, [imagv], [0.0+1.0im]; T=1.0)

    scatter(real.(path), imag.(path))
    xlims!(-1.1, 1.1)
    savefig("test3.png")
end



run()
