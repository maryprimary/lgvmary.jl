#=
测试高斯积分
=#


include("../src/lgvmary.jl")
using ..lgvmary

using Symbolics

using Plots


function run()
    x, imagv, S = fake_action()
    println(S)


    dt = 10/1000
    sum = 0.0
    #实数直接积分
    for rv in range(-5, stop=5, length=1000)
        sum = sum + dt*Symbolics.value(substitute(exp(-S), Dict(x=>rv, imagv=>0.0+1.0im)))
    end
    println(sum)

    sum = 0.0
    ∂S_∂x = Symbolics.derivative(S, x)
    println(∂S_∂x)
    Hes = Symbolics.derivative(∂S_∂x, x)
    println(Hes)
    #在GTA产生的Thimble上积分
    for rv in range(-5, stop=5, length=1000)
        cv, path = inv_Morse(S, ∂S_∂x, x, rv, [imagv], [0.0+1.0im]; T=1.0, steps=2000)
        jaci = inv_Morse_jacobian(S, Hes, 1.0+0.0im, x, path, [imagv], [0.0+1.0im]; T=1.0, steps=2000)
        intgrand = Symbolics.value(substitute(exp(-S), Dict(x=>cv, imagv=>0.0+1.0im)))
        sum = sum + dt*intgrand*jaci
    end
    println(sum)
end


run()
